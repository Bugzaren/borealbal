import pygame
import sys
import numpy as np

from MiscFuncs import *
pygame.init()

#####Hello



dt= 0.01
FrictionList = [0,0.003,0.01]
GravityList = [1.0 , 4.0 , 6.0 ]
TractionList = [0.2, 0.6, 0.9 ]
RotAngleList = [ 0.03 , 0.05, 0.1 ] 
TimeList = [ 60, 180, 300 ]            

SettingsTitles = ("Friction","Gravity","Traction","Rotation","Game Time")
FrictionText = ("Air","Water","Syrup")
GravityText =  ("Moon", "Earth", "Jupiter")
TractionText = ("Glass","Paper", "Rubber")
RotationText = ("Donal Duck","Tom&Jerry","Road Runner")
TimeText = ("1 min","3 min","5 min")
SettingsText = [FrictionText,GravityText,TractionText,RotationText,TimeText]

Radia = 1.0

PixWidth = 600
PixHeight = 600

PlotWidth = 4.0
PlotHeight = 4.0

BallSize = 0.1
TargetSize=0.1
EquatorPointSize=0.01

SideView=True


#!/usr/bin/python
import time
import pickle
import os

DISPLAYSURF = pygame.display.set_mode((PixWidth,PixHeight))


NumSettings = 5


Hits = 0

if os.path.isfile('HighScore.pkl'):
    HighScoreDict = pickle.load( open( "HighScore.pkl", "rb" ) )
else:
    HighScoreDict = dict()



if os.path.isfile('Settings.pkl'):
    SettingChoise = pickle.load( open( "Settings.pkl", "rb" ) )
else:
    SettingChoise=np.zeros(NumSettings,dtype=int)
    SettingChoise[0]=1
    SettingChoise[1]=1
    SettingChoise[2]=1
    SettingChoise[3]=1


    
SettingLook = 0

EquatorRaw, NumEq1Points = CreateBaseEquator()

History = np.zeros((3,30))
History[2,:]=-1



BLACK = pygame.Color(0, 0, 0)
GREY = pygame.Color(150, 150, 150)
WHITE = pygame.Color(255, 255, 255)        
RED = pygame.Color(255, 0, 0)
ColBall = pygame.Color(200, 50, 200)
ColBallD = pygame.Color(100, 25, 100)        
BLUE = pygame.Color(0,0 , 255)
GREEN = pygame.Color(0, 255, 0)        
COL0 = pygame.Color(0,255 , 0)        
COL1 = pygame.Color(0,0 , 255)        
COL2 = pygame.Color(255, 0 , 0)        
COL3 = pygame.Color(200,200 , 50)        
COL0D = pygame.Color(0,100 , 0)        
COL1D = pygame.Color(0,0 , 100)        
COL2D = pygame.Color(100, 0 , 0)        
COL3D = pygame.Color(50,50 , 25)        

TargetColor=[COL0,COL1,COL2,COL3]
TargetColorD=[COL0D,COL1D,COL2D,COL3D]

FPS = pygame.time.Clock()

TargetID = 2
EnemyID = 3

ScoreFont = pygame.font.SysFont('Comic Sans MS', 30)
KeysFont = pygame.font.SysFont('Comic Sans MS', 20)


ScreenNo = 0 ###Start with the start screen




def PosToPix(XPos,YPos):
    ####We assume the plot has the origin center, and then we build the other things around it
    XPix =  int(np.ceil(PixWidth * (.5 + (XPos/PlotWidth))))
    YPix =  int(np.ceil(PixHeight * (.5 - (YPos/PlotHeight))))
    return XPix,YPix


def ScaleToPix(Scale):
    ####We assume the plot has the origin center, and then we build the other things around it
    return int(np.ceil(PixWidth*Scale/PlotWidth))


def DrawVecToPix(Vec,Col,ColD,CircSize):
    PixPos=PosToPix(Vec[0],Vec[2])
    #print("Vec:",Vec)
    ObserveDist=3
    RelSize = CircSize*(Vec[1]+ObserveDist)/ObserveDist
    PixSize = ScaleToPix(RelSize)
    ColMix = (Vec[1]+1)/2.0
    AvCol=Col*ColMix + ColD*(1-ColMix)
    #print("Col:",Col)
    pygame.draw.circle(DISPLAYSURF, AvCol, PixPos, PixSize)


def DrawVecHZToPix(Vec,Col,ColD,CircSize):
    PixPos=PosToPix(Vec[0],Vec[1])
    ObserveDist=3
    RelSize = CircSize*(Vec[2]+ObserveDist)/ObserveDist
    PixSize = ScaleToPix(RelSize)
    #print("Vec:",Vec)

    ColMix = (Vec[2]+1)/2.0
    #print("ColMix:",ColMix)
    AvCol=Col*(1-ColMix) + ColD*ColMix
    #print("Col:",Col)
    #print("AvCol:",AvCol)

    pygame.draw.circle(DISPLAYSURF, AvCol, PixPos, PixSize)


def StartTheGame():
    global StartTime, Hits, Position, Speed, TargetNodes, HITCOUNTER
    global SettingChoise,TractionList
    global ROTX, ROTIX, ROTZ, ROTIZ, ROTC, ROTIC
    global ROTXB, ROTIXB, ROTZB, ROTIZB, ROTCB, ROTICB
    StartTime = time.time()
    Hits = 0
    Position = np.zeros(3)
    Position[2] = -1
    Speed = np.zeros(3)
    HITCOUNTER = 0

    TargetNodes = CreateTargetNodes()

    BallTraction = TractionList[SettingChoise[2]]

    RotAngle = RotAngleList[SettingChoise[3]] 
    ROTX = RotMatX(RotAngle)
    ROTIX = RotMatX(-RotAngle)
    
    ROTZ = RotMatZ(-RotAngle)
    ROTIZ = RotMatZ(RotAngle)

    ROTC = RotMatY(RotAngle)
    ROTIC = RotMatY(-RotAngle)

    ROTXB = RotMatX(RotAngle*BallTraction)
    ROTIXB = RotMatX(-RotAngle*BallTraction)

    ROTZB = RotMatZ(-RotAngle*BallTraction)
    ROTIZB = RotMatZ(RotAngle*BallTraction)

    ROTCB = RotMatY(RotAngle*BallTraction)
    ROTICB = RotMatY(-RotAngle*BallTraction)

    

def StartScreen():
    global ScreenNo, Hits, HighScore, HighScoreDict

    if not tuple(SettingChoise) in HighScoreDict:
        HighScoreDict[tuple(SettingChoise)] = 0
        HighScore = 0
    else:
        HighScore = HighScoreDict[tuple(SettingChoise)]
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()
            sys.exit()
        elif event.type == pygame.KEYDOWN:
            #print(event)
            if event.key == pygame.K_q: ###Quit
                pygame.quit()
                sys.exit()
            elif event.key == pygame.K_s: ###Start the game
                StartTheGame()
                ScreenNo = 1
            elif event.key == pygame.K_c: ###Change the settings
                ScreenNo = 3
            elif event.key == pygame.K_r: ###Change the settings
                HighScoreDict[tuple(SettingChoise)] = 0

                
    DISPLAYSURF.fill(WHITE)


    WelcomeSurface = ScoreFont.render("Welcome to KnuttelBall",
                                      False, BLACK)
    DISPLAYSURF.blit(WelcomeSurface,(50,50))
       
    ScoreSurface = ScoreFont.render("Your latest score was:  "+str(Hits),
                                    False, RED)
    DISPLAYSURF.blit(ScoreSurface,(50,100))
    
    HScoreSurface = ScoreFont.render("The High Score is: "+str(HighScore),
                                     False, BLUE)
    DISPLAYSURF.blit(HScoreSurface,(50,200))
    
    TextSurface = ScoreFont.render("Press (s)tart, (q)uit, (c)hange setting",
                                   False, BLACK)
    DISPLAYSURF.blit(TextSurface,(50,300))

    TextSurface = ScoreFont.render("(r)eset the high score",
                                   False, BLACK)
    DISPLAYSURF.blit(TextSurface,(50,400))


    
def PauseScreen():
    global ScreenNo, StartTime, PauseTime
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()
            sys.exit()
        elif event.type == pygame.KEYDOWN:
            #print(event)
            if event.key == pygame.K_q: ###Quit
                pygame.quit()
                sys.exit()
            elif event.key == pygame.K_r: ###Quit
                UnPauseTime = time.time()
                StartTime += UnPauseTime - PauseTime
                ScreenNo = 1
    DISPLAYSURF.fill(WHITE)


    WelcomeSurface = ScoreFont.render("Game is Paused", False, (0, 0, 0))
    ChoiseSurface = ScoreFont.render("Press (r)esume, (q)uit", False, (0, 0, 0))
    
    DISPLAYSURF.blit(WelcomeSurface,(50,50))
    DISPLAYSURF.blit(ChoiseSurface,(50,300))



def SettingsScreen():
    global ScreenNo, SettingChoise, SettingLook
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()
            sys.exit()
        elif event.type == pygame.KEYDOWN:
            #print(event)
            if event.key == pygame.K_r: ###Return to start page
                pickle.dump( SettingChoise, open( "Settings.pkl", "wb" ) )
                ScreenNo = 0
            elif event.key == pygame.K_q: ###Return to start page
                pygame.quit()
                sys.exit()
            elif event.key == pygame.K_DOWN:
                if SettingLook < (NumSettings-1):
                    SettingLook +=1
            elif event.key == pygame.K_UP: 
                if 0 < SettingLook:
                    SettingLook -=1
            elif event.key == pygame.K_RIGHT:
                if SettingChoise[SettingLook] < 2:
                    SettingChoise[SettingLook] +=1
            elif event.key == pygame.K_LEFT: 
                if 0 < SettingChoise[SettingLook]:
                    SettingChoise[SettingLook] -=1


    DISPLAYSURF.fill(WHITE)
                


    for SetNo in range(NumSettings):
        SP1,SP2=PosToPix(-1.5,1.6-SetNo*0.7)
        if SetNo == SettingLook:
            COL = GREY 
        else:
            COL = BLUE 

        pygame.draw.rect(DISPLAYSURF, COL, (SP1,SP2,ScaleToPix(3),ScaleToPix(.5)))
        ####The text for the title
        SP1,SP2=PosToPix(-1.7,1.8-SetNo*0.7)
        ChoiseSurface = KeysFont.render(SettingsTitles[SetNo], False, BLACK)
        DISPLAYSURF.blit(ChoiseSurface,(SP1,SP2))

        for OptNo in range(3):
            SP1,SP2=PosToPix(-1.4+OptNo,1.55-SetNo*0.7)
            if OptNo == SettingChoise[SetNo]:
                COL = GREEN 
            else:
                COL = RED 
            pygame.draw.rect(DISPLAYSURF, COL, (SP1,SP2,ScaleToPix(0.8),
                                                ScaleToPix(.4)))

            SP1,SP2=PosToPix(-1.37+OptNo,1.45-SetNo*0.7)            
            ChoiseText = KeysFont.render(SettingsText[SetNo][OptNo], False, BLACK)
            DISPLAYSURF.blit(ChoiseText,(SP1,SP2))

    
    WelcomeSurface = ScoreFont.render("The settings", False, BLACK)
    DISPLAYSURF.blit(WelcomeSurface,(50,00))

    ChoiseSurface = ScoreFont.render("Press (r)return to save settings", False, BLACK)
    DISPLAYSURF.blit(ChoiseSurface,(50,550))
    
    

    
def ApplyGravity():
    global Position, GravityVec, Speed, FrictionList,SettingChoise
    global GravityList
    GravityVec = np.array((0,0,-GravityList[SettingChoise[1]]))

    GravProj = np.sum(GravityVec * GravityVec )
    EffGrav =  GravityVec - GravProj*Position
    #print("EffGrav: ",EffGrav)
    
    NewPosition = Position + dt*Speed
    Speed = Speed + dt*EffGrav

    Position = NormVec(NewPosition,Radia)
    #print("Position: ",Position)


    SpeedProj = np.sum(Position * Speed )
    Speed =  Speed - SpeedProj*Position
    Friction = 1 - FrictionList[SettingChoise[0]]
    Speed = Speed * Friction
    #print("Speed: ",Speed)


def RotateTheSphere():
    global TargetNodes, Position, Speed
    if RotLeft:
        TargetNodes = np.matmul(ROTZ,TargetNodes)
        Position = np.matmul(ROTZB,Position)
        Speed = np.matmul(ROTZB,Speed)        
    if RotUp:
        TargetNodes = np.matmul(ROTIX,TargetNodes)
        Position = np.matmul(ROTIXB,Position)
        Speed = np.matmul(ROTIXB,Speed)
    if RotClock:
        TargetNodes = np.matmul(ROTC,TargetNodes)
        Position = np.matmul(ROTCB,Position)
        Speed = np.matmul(ROTCB,Speed)        
    if RotRight:                
        TargetNodes = np.matmul(ROTIZ,TargetNodes)
        Position = np.matmul(ROTIZB,Position)
        Speed = np.matmul(ROTIZB,Speed)        
    if RotDown:
        TargetNodes = np.matmul(ROTX,TargetNodes)
        Position = np.matmul(ROTXB,Position)        
        Speed = np.matmul(ROTXB,Speed)        
    if RotAClock:
        TargetNodes = np.matmul(ROTIC,TargetNodes)
        Position = np.matmul(ROTICB,Position)
        Speed = np.matmul(ROTICB,Speed)        

    

def GameState():
    global RotLeft, RotRight, RotUp,RotDown, RotClock, RotAClock
    global TargetID, EnemyID, HighScoreDict
    global StartTime, Hits, HighScore
    global ScreenNo, PauseTime, HITCOUNTER
    global TargetNodes,Position,SideView
    PressedKeys = pygame.key.get_pressed()
    RotUp=PressedKeys[pygame.K_DOWN]
    RotDown=PressedKeys[pygame.K_UP]
    if SideView:
        RotLeft=PressedKeys[pygame.K_RIGHT]
        RotRight=PressedKeys[pygame.K_LEFT]
        RotClock=PressedKeys[pygame.K_a]
        RotAClock=PressedKeys[pygame.K_d]
    else: ###Top view
        RotLeft=PressedKeys[pygame.K_a] ###ROtate left
        RotRight=PressedKeys[pygame.K_d]
        RotClock=PressedKeys[pygame.K_RIGHT]
        RotAClock=PressedKeys[pygame.K_LEFT]

    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()
            sys.exit()
        elif event.type == pygame.KEYDOWN:
            #print(event)
            if event.key == pygame.K_p: ###Pause
                ScreenNo = 2
                PauseTime = time.time()
            if event.key == pygame.K_v: ###Pause
                SideView=not SideView ###Change view
            elif event.key == pygame.K_q: ###Quit
                if Hits > HighScore:
                    HighScore = Hits
                    ###Save the current High score dict
                    HighScoreDict[tuple(SettingChoise)] = HighScore
                    pickle.dump( HighScoreDict, open( "HighScore.pkl", "wb" ) )

                ScreenNo = 0

    RotateTheSphere() ###Apply the rotations from the keys
    ApplyGravity() ###Apply gravity and move the ball to the next position


    ##We now choose our view
    ##If we do nothing we will look from the top,
    ##But we can also look from the side
    if SideView:
        ToSideView=RotMatX(np.pi/2)
        TargetNodes=np.matmul(ToSideView,TargetNodes)
        Position=np.matmul(ToSideView,Position)
    
    
    NumEqPoints=20
    Equator2 = GetEqyator3(TargetNodes,NumEqPoints)
    NumEqPoints=6*NumEqPoints
            

    if HITCOUNTER > 0:
        DISPLAYSURF.fill(GREEN)
        HITCOUNTER -= 1
    elif  HITCOUNTER < 0:
        DISPLAYSURF.fill(RED)
        HITCOUNTER += 1
    else:
        DISPLAYSURF.fill(WHITE)
    



    if Norm(Position - TargetNodes[:,TargetID]) < (TargetSize+BallSize):
        Hits += 1
        HITCOUNTER = 10
        while True:
            TargetID = np.mod(TargetID+1,4)
            if TargetID!= EnemyID: ###Target and enemy are not the same
                break
    if Norm(Position - TargetNodes[:,EnemyID]) < (TargetSize+BallSize):
        Hits -= 1
        HITCOUNTER = -10
        while True:
            EnemyID = np.mod(EnemyID+1,4)
            if TargetID!= EnemyID: ###Target and enemy are not the same
                break
    
    pygame.draw.circle(DISPLAYSURF, BLUE, PosToPix(0,0),
                       ScaleToPix(1),
                       ScaleToPix(0.01))


    ####Get the agular coordiantes of the pall position
    AngAz,AngPol = GetAngularCoords(Position)    


    ###Here we add the equator line
    RotAz = RotMatY(AngAz)
    ROTPol = RotMatZ(-AngPol+np.pi)
    
    EquatorTrack = np.matmul(ROTPol,np.matmul(RotAz,EquatorRaw))
    
    for indx in range(NumEq1Points):
        DrawVecHZToPix(EquatorTrack[:,indx],BLACK,GREY,EquatorPointSize)
    for indx in range(NumEqPoints):        
        DrawVecHZToPix(Equator2[:,indx],GREY,BLACK,EquatorPointSize)



        

    DepthNodes = np.zeros(5)
    DepthNodes[:4]=TargetNodes[2,:]
    DepthNodes[4]=Position[2]

    DepthOrder = np.argsort(DepthNodes)

    TargetCon = GetConnectionLine(Position,TargetNodes[:,TargetID],12)
    EnemyCon = GetConnectionLine(Position,TargetNodes[:,EnemyID],12)
    for indx in range(12):        
        DrawVecHZToPix(TargetCon[:,indx],GREEN,GREEN,EquatorPointSize)
        DrawVecHZToPix(EnemyCon[:,indx],RED,RED,EquatorPointSize)
    
    DrawVecHZToPix(TargetNodes[:,TargetID],BLACK,BLACK,1.1*TargetSize)
    DrawVecHZToPix(TargetNodes[:,EnemyID],GREY,GREY,1.1*TargetSize)
    for DO in DepthOrder:
        if DO == 4: ##The Ball
            for indx in range(5):
                DrawVecHZToPix(History[:,5*indx],GREY,GREY,(5-indx)*BallSize/5)
            DrawVecHZToPix(Position,ColBall,ColBallD,BallSize)
        else:
            DrawVecHZToPix(TargetNodes[:,DO],TargetColor[DO],TargetColorD[DO],TargetSize)

    ElapsedTime = time.time() - StartTime

    ScoreSurface = ScoreFont.render("Points: "+str(Hits), False, BLACK)
    NextSurface = ScoreFont.render("Hit", False, (0, 255, 0))
    HighSurface = ScoreFont.render("Highscore: "+str(HighScore), False, BLACK)
    ViweInfoSurface = ScoreFont.render("Toggle (v)iew:", False, BLACK)
    if SideView:
        ViweSurface = ScoreFont.render("Side View", False, BLACK)
    else:
        ViweSurface = ScoreFont.render("Top View", False, BLACK)


    EnemySurface = ScoreFont.render("Avoid", False, (250, 0, 0))
    KeysSurface = KeysFont.render("Use a + d + arrow keys, q = quit, p = pause", False, BLACK)

    RoundTime =  TimeList[SettingChoise[4]]           
    
    TimeSurface = ScoreFont.render("Time left: "+str(np.round(RoundTime - ElapsedTime))+" s", False, BLACK)

    if ElapsedTime > RoundTime: ## Rest the counter
        if Hits > HighScore:
            HighScore = Hits
            HighScoreDict[tuple(SettingChoise)] = HighScore
            pickle.dump( HighScoreDict, open( "HighScore.pkl", "wb" ) )
        ScreenNo = 0
        
    
    DISPLAYSURF.blit(ScoreSurface,(10,10))
    DISPLAYSURF.blit(TimeSurface,(10,70))
    DISPLAYSURF.blit(HighSurface,(10,100))
    DISPLAYSURF.blit(ViweInfoSurface,(350,60))   
    DISPLAYSURF.blit(ViweSurface,(450,100))   
    DISPLAYSURF.blit(NextSurface,(150,10))
    DISPLAYSURF.blit(EnemySurface,(150,40))
    DISPLAYSURF.blit(KeysSurface,PosToPix(-1.85,-1.75)) 
    NextPos = (270,35)
    EnemyPos = (270,65)
    pygame.draw.circle(DISPLAYSURF, BLACK, NextPos, 8)   
    pygame.draw.circle(DISPLAYSURF, TargetColor[TargetID], NextPos, 6)
    pygame.draw.circle(DISPLAYSURF, GREY, EnemyPos, 8)
    pygame.draw.circle(DISPLAYSURF, TargetColor[EnemyID], EnemyPos, 6)


    History[:,1:]=History[:,:-1]    
    History[:,0] = 1.0 * Position

    ###If we use the side view we need to rotate back in the end
    if SideView:
        BackToTopView=RotMatX(-np.pi/2)
        TargetNodes=np.matmul(BackToTopView,TargetNodes)
        Position=np.matmul(BackToTopView,Position)


    
    
while True:
    if ScreenNo == 0:
        StartScreen()
    elif ScreenNo==1:
        GameState()
    elif ScreenNo==2:
        PauseScreen()
    elif ScreenNo==3:
        SettingsScreen()
    else:
        error("Unknwon screen number")

    pygame.display.update()
    FPS.tick(60)
