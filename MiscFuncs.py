import numpy as np
import pygame
import sys

def CreateTargetNodes():
    AzAngle=1.91
    cosRel = np.cos(AzAngle)##Fixme Find This
    sinRel = np.sin(AzAngle)##Fixme Find This
    RelAngle = 2*np.pi/3
    cosRel1 = np.cos(RelAngle)
    sinRel1 = np.sin(RelAngle)
    cosRel2 = np.cos(2*RelAngle)
    sinRel2 = np.sin(2*RelAngle)

    TargetNodes = np.zeros((3,4))
    TargetNodes[:,0] = (0,0,1)
    TargetNodes[:,1] = (0,sinRel,cosRel)
    TargetNodes[:,2] = (sinRel1*sinRel,cosRel1*sinRel,cosRel)
    TargetNodes[:,3] = (sinRel2*sinRel,cosRel2*sinRel,cosRel)
    return TargetNodes


def CreateBaseEquator():
    NumEq1Points=30
    Equator=np.zeros((3,2*NumEq1Points))
    Equator[0,:NumEq1Points]=0
    Equator[1,:NumEq1Points]=np.sin(np.linspace(2*np.pi/3,4*np.pi/3,NumEq1Points))
    Equator[2,:NumEq1Points]=np.cos(np.linspace(2*np.pi/3,4*np.pi/3,NumEq1Points))
    Equator[0,NumEq1Points:]=np.sin(np.linspace(2*np.pi/3,4*np.pi/3,NumEq1Points))
    Equator[1,NumEq1Points:]=0
    Equator[2,NumEq1Points:]=np.cos(np.linspace(2*np.pi/3,4*np.pi/3,NumEq1Points))
    NumEq1Points=2*NumEq1Points
    return Equator, NumEq1Points


def NormVec(Vec,Radia):
    NormVec=Radia * Vec/Norm(Vec)
    return NormVec



def Norm(Vec):
    return np.sqrt(np.sum(Vec * Vec))
    

def RotMatX(Angle):
    ROTX = np.zeros((3,3))
    ROTX[1,1] = np.cos(Angle)
    ROTX[0,0] = 1
    ROTX[2,2] = np.cos(Angle)
    ROTX[1,2] = np.sin(Angle)
    ROTX[2,1] = -np.sin(Angle)
    return ROTX

    
def RotMatY(Angle):
    ROTC = np.zeros((3,3))
    ROTC[0,0] = np.cos(Angle)
    ROTC[2,2] = np.cos(Angle)
    ROTC[0,2] = np.sin(Angle)
    ROTC[2,0] = -np.sin(Angle)
    ROTC[1,1] = 1
    return ROTC 

    
def RotMatZ(Angle):
    ROTZ = np.zeros((3,3))
    ROTZ[0,0] = np.cos(Angle)
    ROTZ[1,1] = np.cos(Angle)
    ROTZ[0,1] = np.sin(Angle)
    ROTZ[1,0] = -np.sin(Angle)
    ROTZ[2,2] = 1
    return ROTZ


def GetAngularCoords(Vec):
    ###WE will do a trick an use complex coords
    Z1 = Vec[2] + 1j*np.sqrt(Norm(Vec)**2 - Norm(Vec[2])**2)
    #Print("Z:",Z1)
    Azimut = np.angle(Z1)
    #print("Azimut:",Azimut)
    Z2 = Vec[0] + 1j*Vec[1]
    Polar = np.angle(Z2)
    #print("Polar:",Polar)
    return Azimut,Polar


def GetEqyator3(TargetNodes,NumEqPoints):
    Equator2=np.zeros((3,6*NumEqPoints))
    t = np.linspace(0.0,1.0,NumEqPoints)
    Equator2[:,(0*NumEqPoints):(1*NumEqPoints)]=(
        GetConnectionLine(TargetNodes[:,0],TargetNodes[:,1],NumEqPoints))
    for indx in range(3):
        Equator2[indx,(1*NumEqPoints):(2*NumEqPoints)] = TargetNodes[indx,0]*t + TargetNodes[indx,2]*(1-t)
        Equator2[indx,(2*NumEqPoints):(3*NumEqPoints)] = TargetNodes[indx,0]*t + TargetNodes[indx,3]*(1-t)
        Equator2[indx,(3*NumEqPoints):(4*NumEqPoints)] = TargetNodes[indx,1]*t + TargetNodes[indx,2]*(1-t)
        Equator2[indx,(4*NumEqPoints):(5*NumEqPoints)] = TargetNodes[indx,1]*t + TargetNodes[indx,3]*(1-t)
        Equator2[indx,(5*NumEqPoints):(6*NumEqPoints)] = TargetNodes[indx,2]*t + TargetNodes[indx,3]*(1-t)

    return Equator2



def GetConnectionLine(Vec1,Vec2,NumEqPoints):
    ConnecLines=np.zeros((3,NumEqPoints))
    t = np.linspace(0.0,1.0,NumEqPoints)
    for indx in range(3):
        ConnecLines[indx,:] = Vec1[indx]*t + Vec2[indx]*(1-t)
    return ConnecLines
