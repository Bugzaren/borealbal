import sys
from cx_Freeze import setup, Executable

exe = Executable(
    script=r"Ball.py",
    base="Win32GUI",
    )

setup(
    name = "BorealBall",
    version = "0.1",
    description = "Bas ball game",
    executables = [exe]
    )
