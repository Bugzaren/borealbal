In Knuttelbal you will rotate a sphere with a ball moving on it's surface. The ball on the surface will be affected by gravity and friction. The aim of the game is to roll the ball such that it touches targets of different colour.

To run you will need Python3, `numpy` and `pygame` installed.

One those are in place open a terminart pointing at the folder where the files are and type

    python3 Ball.py

A start window will then appear where you can choose to: start the game (press s), quit (press q), change the settings (press c).

Enjoy!